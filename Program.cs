﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variable that stores the initial value of the loop.
            bool controlValue = true;
            //This while loop will repeat itself until you enter the "n" as an answer. 
            //This allows the program to be tested several times without having to debug from Visual Studio everytime.
            while (controlValue)
            {
                //First we clean the console.
                Console.Clear();
                //We then call the static GetData method which prompts the user to enter a palindrome and stores it inside of a string.
                string response = Program.GetData();
                //We can now call the DetectPalindrome method to tell us if we are dealing with a legit palindrome.
                //We store the value in a bool variable.
                bool isPalindrome = Program.DetectPalindrome(response);
                //I create a new instance of Program so that I can call the void Method DisplayResults()
                Program pp = new Program();
                //I pass the the user input (response) and the result of the palindrome evaluation (isPalindrome)
                //into the DisplayResults method that will just display the results of the calculations.
                pp.DisplayResults(response, isPalindrome);
                //I ask the user if they want to repeat the program to test an additional word.
                Console.WriteLine("Do you want to continue? y or n");
                //we store the user's response inside of a variable called exitValue.
                string exitValue = Console.ReadLine();
                //We turn the string value to lower case so that we can compare it with the hardcoded "n"
                //if the value is indeed  "n", we return false to exit the loop.
                if (exitValue.ToLower() == "n")
                {
                    controlValue = false;
                }
            }

        }
        //This is a static method that return a string containing the word to be evaluated.
        public static string GetData()
        {
            //We prompt the user to enter some data.
            Console.Write("Enter a palindrome: ");
            //We read the answer from the console and store it inside of a string.
            string answer = Console.ReadLine();
            //we return this string.
            return answer;
        }
        //I spent lots of time trying to figure this mehotd out but finally gave up!
        //I found it on http://www.dotnetperls.com/palindrome
        //The method takes Sarah Palin as a parameter (the Palin-Drome to be evaluated ;))
        public static bool DetectPalindrome(string sarahPalin)
        {
          
            //This was clever we define two int variables to establish our two starting locations.
            //int min is started as zero and the max value is obtained by calling the extension method
            //length and substracting the int that is returned by 1.
            int min = 0;
            int max = sarahPalin.Length - 1;

            //We define the loop structure and started as true to guarantee that it will repeat itself at least once.
            while (true)
            {

                //if at any point the minimum value (which will serve as our array index) happens to be
                //greater than the max value, then we are dealing with a Palindrome.                
                if (min > max)
                {
                    return true;
                }

                //THIS IS SO COOL! We store the first letter on the string on a char variable and we use
                //the min value as its index so that our starting point is the first letter of the string.
                char a = sarahPalin[min];
                //We do the same for the last word of the string. We store the last word of the string in
                //a char object with an index number that maps to the last word in the string being evaluated.
                char b = sarahPalin[max];
                //We evaluate whatever was entered as a word by first converting the data entered to a lower case
                //(the user could have entered capital letters but here he is making all things equal )
                //IF AT ANY TIME ... the letters being evaluated from index zero forward and from the last index
                //backwards do not equal each other, we need to return false to exit the while loop.
                if (char.ToLower(a) != char.ToLower(b))
                {
                    return false;
                }
                //starts from the zero and moves forwards
                min++;
                //starts from the last letter in the string and moves backwards.
                max--;
            }

        }
        //A simple method that takes the initial string entered and the results as parameters
        //so that it can display the word that was entered and its results.
        public void DisplayResults(string word, bool resultado)
        {
            Console.WriteLine("The word '{0}' evaluated as {1}.", word, resultado);
        }
    }
}